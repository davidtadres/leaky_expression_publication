# leaky_expression_publication

# Data collection

Data was collected using two different microscopes: All larval data
(Figure 1 and 4) was collected on a Hyperscope (Scientifica). The data
of the adult (Figure 3) was collected on a Bergamo II (Thorlabs).

# Analysis Pipeline

## Larval data (Figure 1 & 4)

The dF/F was extracted from the raw data using the 'calc_dF_F_looped.py' 
script. 

The output of that script is a csv file containing the dF/F value for 
each repeat. 

## Adult data (Figure 3)

Time series of stack images underwent rigid motion correction with 
NoRMCorre (Pnevmatikakis and Giovannucci, 2017). These images were 
averaged across z-planes for the following analyses. ROI was manually 
defined based on the images averaged across the entire recording.

## Rest of pipeline

To put all data into a manageable format, 'combine_data.ipynb' pulls
all data for a given figure together into a single numpy array while
adding metadata. See the script for details. Since this data has a 
manageable size it is provided with this repository.

Finally, each indicated script will prepare the indicated plots. For 
example, "Fig 1 (attP1 traces).ipynb" will contain the code to produce
all plots related to the attP1 landing site in Figure 1.


## prep environment

`conda create -n leaky_expression_env python=3.8`

Activate the environment:

Windows:

`activate leaky_expression_env`

MacOS

`source activate leaky_expression_env`

Install the necessary packages:

`conda install matplotlib -y`

`conda install -c anaconda scipy -y`

`conda install -c conda-forge imageio -y`

`conda install -c anaconda jupyter -y`

`conda install pandas -y`

`conda install natsort -y`

`conda install -c conda-forge statsmodels -y`

`conda install -c anaconda xlrd -y`

`conda install conda-forge::scikit-posthocs -y`

If you want to work with the raw tiff files of Figure 1 & 3:

`pip install scanimage-tiff-reader`

Then type 

`jupyter notebook`

and navigate to this folder to select the script of interest.