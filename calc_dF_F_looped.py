"""
This script was used on the raw tiff files from the Scientifica scope
for the larval data (Figure 1 & 3).
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import json
from pathlib import Path
from tkinter import filedialog, Tk
from ScanImageTiffReader import ScanImageTiffReader
import matplotlib.patches as patches
import natsort
import pandas as pd

number_of_roi = 1 # current max is 3

STIM_SAMPLE_RATE = 1000 # frequency in Hz

root = Tk()
root.withdraw()
image_path = filedialog.askdirectory(title='Select directory with images.')

class AnnotateRectangle(object):
    """
    This class lets the user select a rectangle on a matplotlib plot by
    first pressing the mouse button on one edge and releasing the mouse
    button at another edge. It saves the x and y coordinates of both
    clicks to define the region of interest.
    """
    def __init__(self):
        self.ax = plt.gca()
        self.x0 = None
        self.y0 = None
        self.x1 = None
        self.y1 = None
        self.rect = Rectangle((0, 0), 1, 1, hatch='//', fill=False)  # initiate the rectangle
        self.ax.add_patch(self.rect)  # draw the rectangle
        self.ax.figure.canvas.mpl_connect('button_press_event', self.on_press)
        self.ax.figure.canvas.mpl_connect('button_release_event', self.on_release)

    def on_press(self, event):
        print('press')
        self.x0 = event.xdata
        self.y0 = event.ydata

    def on_release(self, event):
        print('released')
        if event.xdata > self.x0:
            self.x1 = event.xdata
        else:
            self.x1 = self.x0.copy()
            self.x0 = event.xdata
        if event.ydata > self.y0:
            self.y1 = event.ydata
        else:
            self.y1 = self.y0.copy()
            self.y0 = event.ydata
        self.rect.set_width(self.x1 - self.x0)
        self.rect.set_height(self.y1 - self.y0)
        self.rect.set_xy((self.x0, self.y0))
        self.ax.figure.canvas.draw()

# collect filenames to read. Need len!
files_to_read = []
for file in Path(image_path).iterdir():
    if 'tif' in str(file):
        files_to_read.append(file)
        #print(file)
    if 'step.npy' in str(file):
        stim_file_path = file
# make sure files are in the right order
files_to_read = natsort.natsorted(files_to_read)

image_stack = None
for counter, file in enumerate(files_to_read):
    # Read Stack, takes long
    vol = ScanImageTiffReader(str(file))
    if image_stack is None:
        temp = vol.data()
        image_stack = np.zeros((temp.shape[0],
                                temp.shape[1],
                                temp.shape[2],
                                len(files_to_read)))
    image_stack[:,:,:,counter] = vol.data()

metadata = vol.metadata()

# properly format this relatively complex metadata
clean_metadata = {}
for i in range(len(vol.metadata().split("\n"))):
    try:
        clean_metadata[vol.metadata().split("\n")[i].split(" =")[0]] = \
            vol.metadata().split("\n")[i].split("= ")[1]
    except IndexError:
        pass

actual_framerate = clean_metadata['SI.hRoiManager.scanFrameRate']

# take the mean image per experiment once for performance
mean_image_stack = np.nanmean(image_stack, axis=(0,3))

# Select ROI to do bleedthrough subtraction
# Let the user define a ROI with the mouse by clicking on one edge of the
# rectangle and release at the opposite side.
datatype = [('x0', float), ('x1', float), ('y0', float), ('y1', float)]
sub_ROIs = np.zeros((int(1)), dtype=datatype)
fig = plt.figure('Select a region that does NOT change')
ax = fig.add_subplot(111)
ax.imshow(mean_image_stack)
roi = AnnotateRectangle()
plt.show(block=True)
sub_ROIs['x0'][0] = roi.x0
sub_ROIs['x1'][0] = roi.x1
sub_ROIs['y0'][0] = roi.y0
sub_ROIs['y1'][0] = roi.y1

background_over_time = np.zeros((image_stack.shape[0],
                                 image_stack.shape[3]))
background_over_time[:] = np.nanmean(
    image_stack[:,
                int(sub_ROIs['y0'][0]):int(sub_ROIs['y1'][0]),
                int(sub_ROIs['x0'][0]):int(sub_ROIs['x1'][0]),
                :],
    axis=(1, 2))

# Let the user define a ROI with the mouse by clicking on one edge of the
# rectangle and release at the opposite side.
roi_over_time = np.zeros((image_stack.shape[0],number_of_roi,
                          image_stack.shape[3]))
subtracted_signal = np.zeros((roi_over_time.shape[0],
                              roi_over_time.shape[1],
                              roi_over_time.shape[2]))
datatype = [('x0', float), ('x1', float), ('y0', float), ('y1', float)]
ROIs = np.zeros((int(number_of_roi)), dtype=datatype)
for i in range(int(number_of_roi)):
    fig = plt.figure('Select a region of interest')
    ax = fig.add_subplot(111)
    ax.imshow(mean_image_stack)
    roi = AnnotateRectangle()
    plt.show(block=True)
    ROIs['x0'][i] = roi.x0
    ROIs['x1'][i] = roi.x1
    ROIs['y0'][i] = roi.y0
    ROIs['y1'][i] = roi.y1

    roi_over_time[:,i,:] = np.nanmean(
        image_stack[:,
        int(ROIs['y0'][i]):int(ROIs['y1'][i]),
        int(ROIs['x0'][i]):int(ROIs['x1'][i])],
        axis=(1, 2))


    # As a first pass, just subtract the background from the signal
    subtracted_signal[:,i,:] = roi_over_time[:,i,:] - \
                              background_over_time

#################################################
# Stim file
# The stimulus protocol saves a npy file which contains the stimulus
# protocol which is read here.
stim = np.load(stim_file_path)

# It should always be better to project the slower signal onto the
# faster signal. Otherwise I might run into problems if the fast
# signal changes faster than the slower, e.g. if I were to present a
# 50Hz stimulus
frame_to_stim_conv = STIM_SAMPLE_RATE/float(actual_framerate)
stretched_signal = np.zeros((stim.shape[0], subtracted_signal.shape[1],
                             subtracted_signal.shape[2]))

current_index = 0
for frame in range(subtracted_signal.shape[0]):
    stretched_signal[
        int(round(current_index)):int(round(current_index+frame_to_stim_conv)),:,:] = \
            subtracted_signal[frame,:,:]

    current_index += frame_to_stim_conv

# Rename variable for easy use
F = stretched_signal

# For now I hardcode the exact time the stimulus starts and also
# that the script should take 2 seconds before for F_zero
F_zero_indeces = np.arange(0,3000,1)
F_zero = np.zeros((subtracted_signal.shape[1],
                   stretched_signal.shape[2]))
for i_roi in range(F_zero.shape[0]):
    F_zero[i_roi,:] = np.nanmean(stretched_signal[F_zero_indeces, i_roi, :], axis=0)

delta_F_over_F = ((F-F_zero)/F_zero)

fig = plt.figure()
ax_top = fig.add_subplot(211)
ax_bottom = fig.add_subplot(212)
# plot top right (mean image with indication of ROI
ax_top.imshow(mean_image_stack, cmap='hot')
ax_top.add_patch(
    patches.Rectangle(
        (sub_ROIs['x0'][0], sub_ROIs['y0'][0]),  # (x,y)
        sub_ROIs['x1'][0] - sub_ROIs['x0'][0],  # width
        sub_ROIs['y1'][0] - sub_ROIs['y0'][0],  # height
        fill=False,
        edgecolor='g',
        linewidth=3
    )
)

# Now go for the actual signal.
colors = ['b', 'c', 'm']
# For easy y label prepare correct time in seconds
time = np.linspace(0, stim.shape[0]/STIM_SAMPLE_RATE,stim.shape[0])
# if first ROI, pre-allocate an empty array with the number of images so that each timepoint for which we have an
# image can get a single value for the raw fluorescence measured over the whole experiment

for i_roi in range(int(number_of_roi)):
    ax_top.add_patch(
        patches.Rectangle(
            (ROIs['x0'][i_roi], ROIs['y0'][i_roi]),  # (x,y)
            ROIs['x1'][i_roi] - ROIs['x0'][i_roi],  # width
            ROIs['y1'][i_roi] - ROIs['y0'][i_roi],  # height
            fill=False,
            edgecolor=colors[i_roi],
            linewidth=3
        )
    )

    # As a first pass, just subtract the background from the signal
    #subtracted_signal = roi_over_time[:,i_roi,:] - background_over_time

    # since we have several repeats loop over those. To differentiate
    # I'll put different alpha values
    # WILL ONLY WORK WITH 4 REPEATS LIKE THIS
    for aquisition in range(stretched_signal.shape[2]):
        ax_bottom.plot(time, delta_F_over_F[:,i_roi,aquisition], colors[i_roi],
                       alpha=0.3)



ax_bottom.set_ylabel(r'$\Delta F/F$, F zero :'
              + repr(F_zero_indeces.shape[0]) + 'ms')


ax_bottom.set_xlabel('Time[s]')

# plot stim
ax_bottom_right = ax_bottom.twinx()
ax_bottom_right.plot(time, stim)
ax_bottom_right.set_ylabel('Stim [V]')

fig.savefig(Path(str(Path(image_path)),
                 Path(image_path).name + 'dF_over_F.png'))

# For saving as a csv file.
if number_of_roi == 1:
    dataframe = pd.DataFrame()
    dataframe['Time [s]'] = time
    dataframe['Stimulation [V]'] = stim
    for i_loop in range(delta_F_over_F.shape[2]):
        dataframe['dF/F no ' + repr(i_loop + 1)] = \
            delta_F_over_F[:,0,i_loop]

    dataframe.to_csv(Path(str(Path(image_path)),
                          Path(image_path).name + 'delta_F_over_F.csv'))

# save the ROI for future reference!
np.save(Path(str(Path(image_path)), 'background_ROI.npy'), sub_ROIs)
np.save(Path(str(Path(image_path)), 'signal_ROI.npy'), ROIs)

























