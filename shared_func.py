"""
The goal of these shared functions file is to make sure every plot
is created with exactly the same parameters.
"""

import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

from scipy.signal import savgol_filter
import pandas as pd
import os
from scipy import stats
import statsmodels.api as sm

home_path = os.getcwd()

def prepare_numpy_array(folders, stim_to_use, filepath,
                       savepath, adult, savename, adult_animals=6, no_of_animals=5):
    """
    This function collects the dF/F from
    """
    final_array = None
    if adult:
        n_animals = adult_animals # n=6 for adult experiments except
        # for calyx where n == 4
    else:
        n_animals = no_of_animals

    # Adult and larvae are in slighty different folder structure.
    # Genotype and (ATR+ and ATR-)
    # Defined through 'folders' list
    for folder_counter, current_folder in enumerate(folders):
        print(current_folder)
        # stimulus strength and length
        # Defined in 'stim_to_use'
        for stim_counter in range(len(stim_to_use)):
            subject_counter = 0
            stim_folder = Path(filepath, current_folder, stim_to_use[stim_counter])
            print(stim_folder)
            # For each experiment (each from each animal)...
            if not adult:
                for current_experiment in stim_folder.iterdir():
                    if current_experiment.is_dir():
                        # ...check every file for the csv containing the dF/F data.
                        for current_file in current_experiment.iterdir():
                            if 'dF_over_F.csv' in current_file.name:
                                # Read the csv file
                                data = pd.read_csv(current_file)
                                # Drop the meaningless first column
                                data = data.drop('Unnamed: 0', axis=1)

                                if final_array is None:
                                    # array for larva will have 600, 7, 5 , "no of stim_to_use", "no of folders"
                                    final_array = np.zeros(
                                        (data.shape[0],
                                         data.shape[1],
                                         n_animals,
                                         len(stim_to_use),
                                         len(folders)))

                                final_array[:, :, subject_counter, stim_counter, folder_counter] = data

                                subject_counter += 1
            # For adult the folder structure is slighty different.
            # I dodn't want to another function as it will obscure the
            # code but the idea is that the only difference between the
            # above and the below is how the data is found. Otherewise
            # everything is treated identically (everything after
            # data = pd.read_csv... is identical)
            else:
                for i_files in stim_folder.iterdir():
                    if 'csv' in i_files.name:
                        data=pd.read_csv(i_files)
                        #print(data)

                        # Drop the meaningless first column
                        data = data.drop('Unnamed: 0', axis=1)

                        if final_array is None:
                            # array for larva will have 600, 7, 5 , "no of stim_to_use", "no of folders"
                            final_array = np.zeros(
                                (data.shape[0],
                                 data.shape[1],
                                 n_animals,
                                 len(stim_to_use),
                                 len(folders)))

                        final_array[:, :, subject_counter, stim_counter, folder_counter] = data

                        subject_counter += 1

    print(final_array.shape)
    np.save(Path(savepath, savename), final_array)

def plot_connected_scatterplot(ax,
                               median_before_stim,
                               median_during_stim,
                               light_intensity,
                               color,
                               start_x_position,
                               group=0,):

    for i in range(median_before_stim.shape[0]):
        x_left = np.random.normal(start_x_position, 0.005, size=1)
        x_right = np.random.normal(start_x_position + 0.25, 0.0051,
                                   size=1)

        y_left = median_before_stim[i, light_intensity, group]
        y_right = median_during_stim[i, light_intensity, group]

        ax.scatter(x_left, y_left, color=color, alpha=0.5)
        if i == 0:
            ax.scatter(x_right, y_right, color=color, alpha=0.5)
        else:
            ax.scatter(x_right, y_right, color=color, alpha=0.5)

        ax.plot([x_left, x_right], [y_left, y_right], color=color,alpha=0.5)



def holms_bonferroni_correction(p_values, target_alpha=0.05):
    """
    # Holms-Bonferroni Correction:
    https://www.statisticshowto.com/holm-bonferroni-method/

    Takes a list of p-values and performs holms-bonferroni correction
    """
    # Step 1: Order the p-values from smallest to greatest:
    sorted_p_values = np.sort(np.array(p_values))

    current_rank = 1

    # Step 2: Go from lowest to highest p-value and compare
    # to the current alpha
    for current_p in sorted_p_values:
        current_alpha = target_alpha / (
                    sorted_p_values.shape[0] - current_rank + 1)
        if current_p < current_alpha:
            print('significantly different with:')
            print('current p: ' + repr(
                current_p) + ', current alpha: ' + repr(current_alpha))
        else:
            print('NOT DIFFERENT!')
            print('current p: ' + repr(
                current_p) + ', current alpha: ' + repr(current_alpha))

        current_rank += 1


def create_csv_file(light_intensity_index,
                    actual_light_intensity,
                    data,
                    fixed_params,
                    save_path,
                    ATR_exp=True):
    """
    This function grabs the fluorescent traces from Hiroshi's matlab
    file and calculates the dF/F
    """
    time_before = 10  # seconds
    time_after = 10  # seconds

    # convert time to indeces
    time_before *= int(round(fixed_params['framerate']))
    time_after *= int(round(fixed_params['framerate']))

    for i_animal in range(data.shape[0]):
        if ATR_exp:
            ATR = None
            if i_animal % 2 == 0:
                ATR = 'ATR+'
            else:
                ATR = 'ATR-'
        else:
            ATR = False

        csv = pd.DataFrame()

        for i_stim, (stim_start, stim_stop) in enumerate(
                zip(fixed_params['TS_OptStimImg_onset'], fixed_params['TS_OptStimImg_offset'])):
            # explicitly define start and stop index in original file
            take_index_start = int(round(stim_start - time_before))
            take_index_stop = int(round(stim_stop + time_after))

            target_stim_index_start = time_before
            if i_stim == 0:
                # Time is given in seconds from 0 to end.
                # We only take parts of the data so time will change.
                # Since framerate is fixed I can just take time from the first frames!
                csv['Time [s]'] =  fixed_params['TS_Img'][
                                  0: take_index_stop - take_index_start]
                stimulation_in_save = np.zeros(
                    (take_index_stop - take_index_start))
                stimulation_in_save[time_before:int(
                    time_before + stim_stop - stim_start)] = int(
                    actual_light_intensity.split('uW_mm2')[0])
                csv['Stimulation [uW/mm2]'] = stimulation_in_save

                # For now I hardcode
                # that the script should take 2 seconds before stimulus for F_zero
                # Note that F_zero indeces are always the same!
                F_zero_indeces = np.arange(int(round(
                    target_stim_index_start - (2 * fixed_params['framerate']))),
                                           target_stim_index_start, 1)

            F = data[i_animal, 0,
                stim_start - time_before:stim_stop + time_after,
                light_intensity_index]

            F_zero = np.nanmean(F[F_zero_indeces])
            # F_zero = np.zeros(subtracted_signal.shape[1])
            # for i in range(F_zero.shape[0]):
            #    F_zero[i] = np.nanmean(streched_signal[F_zero_indeces])

            delta_F_over_F = ((F - F_zero) / F_zero)

            csv['dF/F no ' + repr(i_stim + 1)] = delta_F_over_F

        if not ATR:
            current_save_path = Path(save_path, actual_light_intensity)
        else:
            current_save_path = Path(save_path, ATR,
                                     actual_light_intensity)
        current_save_path.mkdir(parents=True, exist_ok=True)

        if not ATR:
            csv.to_csv(Path(current_save_path,
                            actual_light_intensity + '_animal' + repr(
                                i_animal) + '.csv'))
        else:
            csv.to_csv(Path(current_save_path,
                            ATR + '_' + actual_light_intensity + '_animal' + repr(
                                i_animal) + '.csv'))


def pairwise_t_tests(median_during_stim,
                     median_before_stim,
                     p_values,
                     non_parametric=False):
    # paired t-test is essentially a one sample t-test of the difference.
    print('lilliefors: ' + repr(sm.stats.lilliefors(median_before_stim-median_during_stim)[-1]))

    if non_parametric:
        print('Wilcoxon:')
        print(stats.wilcoxon(median_during_stim, median_before_stim))
        print('\n')
        p_values.append(stats.wilcoxon(median_during_stim, median_before_stim)[1])

    else:
        print('T-Test')
        print(stats.ttest_rel(median_during_stim, median_before_stim))
        print('\n')
        p_values.append(stats.ttest_rel(median_during_stim, median_before_stim)[1])


def median_per_animal_func(aq_rate, data):
    """
    This function first filters input data with savitzky-golay filter
    of 1 second.

    It then calculates the median per animal and returns the array
    """

    # FILTER ALL DATA WITH WINDOW 1s
    window_size = aq_rate * 1  # in seconds
    if window_size % 2 == 0:  # Window size must be odd
        window_size += 1
    # make sure window size is an integer
    window_size = int(round(window_size))

    filtered_data = savgol_filter(data[:, 2::, :, :, :],
                                  window_length=window_size,
                                  polyorder=2, axis=0)

    # Next, grab median PER ANIMAL
    median_per_animal = np.median(filtered_data, axis=(1))

    return (median_per_animal)


def plot_individual_animals(data, no_of_animals, window_size,
                            volt_index, color, savepath,
                            exp_index=0, y_lim=None):
    """
    To show intra- and inter-animal variability better we plot the
    individual responses in supplement.
    """
    fig = plt.figure(figsize=(no_of_animals * 1.5, 1.5))

    for i in range(no_of_animals):
        # This is a bit convoluted but since I have some experiments with n=5 and other with n=6
        # and because I want to index axes with integer I have to have e.g. 151 for n=5 and
        # 161 for n=6
        no_for_axes = no_of_animals * 10 + 101
        if i == 0:
            ax = fig.add_subplot(no_for_axes + i)
        else:
            ax = fig.add_subplot(no_for_axes + i, sharey=ax)

        # FILTER ALL DATA WITH WINDOW 1s
        filtered_data = savgol_filter(data[:, 2::, i, volt_index, exp_index],
                                      window_length=window_size,
                                      polyorder=2, axis=0)

        # PLOT DATA
        ax.plot(data[:, 0, i, volt_index, exp_index], filtered_data, c=color,
                lw=0.5, alpha=0.5)
        # PLOT MEDIAN
        ax.plot(data[:, 0, i, volt_index, exp_index],
                np.median(filtered_data, axis=1), c=color, lw=1)

        # Plot stimulus
        start_stim = np.where(data[:, 1, i, volt_index, exp_index] > 0.1)[0][0]
        stop_stim = np.where(data[:, 1, i, volt_index, exp_index] > 0.1)[0][-1]
        ax.axvspan(data[start_stim, 0, i, volt_index, exp_index],
                   data[stop_stim, 0, i, volt_index, exp_index],
                   lw=0, zorder=0,
                   color='k',
                   alpha=0.1)
        if y_lim is not None:
            ax.set_ylim(y_lim)
        fig.tight_layout()

        fig.savefig(savepath)



def export_pairwise_dependent_test(data, label_list,
                                   savepath, savename,
                                   parametric=True):
    """
    This function can be used for a single or multiple pairwise DEPENDENT tests.

    In case of multiple tests (identified as nested lists),
    Bonferroni correction is applied and both the original and
    corrected p-value is reported.
    """

    # First, collect the data of variable names below
    test_statistic = []
    p_values = []
    all_labels = []
    mean = []
    median = []
    std = []
    sem = []
    n = []
    ci = []
    # effect_size = []
    dof = []

    no_of_groups = 0

    for counter, current_dataset in enumerate(data):
        # Explicitly assign left and right group of current data to be compared
        group1_data = current_dataset[0]
        group2_data = current_dataset[1]

        if group1_data.shape[0] != group2_data.shape[0]:
            print('unequal group size!')

        mean.append([np.nanmean(group1_data),
                     np.nanmean(group2_data)])

        median.append([np.nanmedian(group1_data),
                       np.nanmedian(group2_data)])

        std.append([np.std(group1_data),
                    np.std(group2_data)])

        n.append([group1_data.shape[0],
                  group2_data.shape[0]])

        sem.append([np.std(group1_data) / np.sqrt(group1_data.shape[0]),
                    np.std(group2_data) / np.sqrt(group2_data.shape[0])
                    ])

        ci.append([confidence_interval(group1_data),
                   confidence_interval(group2_data)])

        if parametric:
            test_results = stats.ttest_rel(group1_data, group2_data)
        else:
            test_results = stats.wilcoxon(group1_data, group2_data)
        test_statistic.append(test_results[0])
        p_values.append(test_results[-1])
        all_labels.append(label_list[counter])

        dof.append(group1_data.shape[0] - 1)  # paired t test is number of pairs - 1

        no_of_groups += 1

    ###################################
    # Multiple Test correction using Holm-Bonferroni methods
    ###################################

    if len(p_values) > 1:
        corrected_p_values = Holm_bonferroni_pvalue_correction(p_values)

    #########################################
    # Finally, save the dataframe for export
    #########################################
    df_for_export = pd.DataFrame()

    flat_label_list = flatten_list(all_labels)
    df_for_export["Group"] = flat_label_list

    flat_n = flatten_list(n)
    df_for_export['n'] = flat_n

    flat_mean = flatten_list(mean)
    df_for_export['mean'] = flat_mean

    flat_median = flatten_list(median)
    df_for_export['median'] = flat_median

    flat_std = flatten_list(std)
    df_for_export['STD'] = flat_std

    flat_sem = flatten_list(sem)
    df_for_export['SEM'] = flat_sem

    flat_ci = flatten_list(ci)
    df_for_export['95% CI on the mean'] = flat_ci

    # Next, add values that are only added for every second row as we are always comparing two rows to each other
    for counter, current_index in enumerate(range(0, df_for_export.shape[0], 2)):  # only every second row

        df_for_export.loc[current_index, 'DOF'] = dof[counter]

        if parametric:
            df_for_export.loc[current_index, 'statistical test'] = 'T-test rel'
        else:
            df_for_export.loc[
                current_index, 'statistical test'] = 'Wilcoxon test'
        if parametric:
            df_for_export.loc[current_index, 't-statistic'] = test_statistic[counter]
        else:
            df_for_export.loc[current_index, 'Wilcoxon statistic'] = test_statistic[counter]

        df_for_export.loc[current_index, 'p-value'] = p_values[counter]
        if len(p_values) > 1:
            df_for_export.loc[current_index, 'Holm-Bonferroni corrected p-value'] = \
                corrected_p_values[counter]

        # That will still work as expected as the corrected value will
        # be correct even if there's only one group!
        if len(p_values) > 1:
            if corrected_p_values[counter] < 0.05:
                df_for_export.loc[current_index, 'significant'] = 'YES'
                '''
                # Only put effect size in case of significant differences
                # of the group
                if parametric:
                    df_for_export.loc[current_index, "Effect size: Cohen's D"] = effect_size[counter]
                else:
                    df_for_export.loc[current_index, "Effect size: r^2"] = effect_size[counter]
                '''
            else:
                df_for_export.loc[current_index, 'significant'] = 'NO'
                '''
                # Only put effect size in case of significant differences
                # of the group
                df_for_export.loc[current_index, "Effect size"] = 'N/A'
                '''
        else:
            if p_values[counter] < 0.05:
                df_for_export.loc[current_index, 'significant'] = 'YES'
            else:
                df_for_export.loc[current_index, 'significant'] = 'NO'

    # if SAVE_PLOTS:
    if parametric:
        df_for_export.to_csv(Path(savepath, savename + '_t_test.csv'))
    else:
        df_for_export.to_csv(Path(savepath, savename + '_wilcoxon.csv'))

    print(df_for_export)

def export_statistics_multiple(data, label_list,
                               savepath, savename, parametric=False):
    """
    Calculate and prepare all the statistical calculations presented
    in Data S1.
    """
    label_list_for_tukey = label_list.copy() # keep for tukey test
    data_label='NavigationIndex'
    group_label='Group'

    print('\n')

    print(stats.levene(*data))
    print('Export statistics:')

    # For Anova & Kruskal Wallis post-hoc tests, data needs to be
    # in this particular format.
    df_for_stats = pd.DataFrame()
    for i in range(len(label_list)):
        dict_for_stats = {
             group_label: [label_list[i]] * data[i].shape[0],
             data_label: data[i]
        }

        df_to_add = pd.DataFrame(dict_for_stats)

        try:
            df_for_stats = pd.concat([df_to_add, df_for_stats],
                                     ignore_index=True)
            df_for_stats.reset_index()
        except Exception as e:
            print(e)
            df_for_stats = df_to_add

    labels = df_for_stats.drop_duplicates(subset=[group_label])[group_label]  # extract unique group names
    label_list = []
    [label_list.append(i) for i in labels]  # put them into a list
    #print(label_list)  # a confirmation that the group names are entered correctly!

    std = []
    n = []
    mean = []
    median = []
    sem = []
    ci = []
    dof = 0
    lilliefors_test_stat = []
    lilliefors_p = []
    flat_data = []

    for current_label in label_list:
        current_data = df_for_stats[data_label][df_for_stats[group_label] == current_label]

        mean.append(np.nanmean(current_data))
        median.append(np.nanmedian(current_data))
        std.append(np.nanstd(current_data))

        n.append(current_data.shape[0])

        sem.append(std[-1] / np.sqrt(n[-1]))

        ci.append(confidence_interval(current_data))

        dof += current_data.shape[0]

        lilliefors_test_stat.append(
            sm.stats.lilliefors(current_data)[0])
        lilliefors_p.append(sm.stats.lilliefors(current_data)[-1])

        flat_data.append(current_data)

    dof -= len(label_list) # DoF of one way ANOVA is N - k with k being the number of groups

    # Put data into dataframe for easy export
    df_for_export = pd.DataFrame()

    # df_for_export['Group'] = label_list # Needs to be adapted with dilutions!
    df_for_export[group_label] = label_list
    df_for_export['n'] = n
    df_for_export['mean'] = mean
    df_for_export['median'] = median
    df_for_export['STD'] = std
    df_for_export['SEM'] = sem
    df_for_export['95% CI on the mean'] = ci

    df_for_export.loc[0, 'DOF'] = dof

    df_for_export['statistical test'] = pd.Series(dtype='string')
    if parametric:
        df_for_export.loc[0, 'statistical test'] = 'ANOVA'
        statistics = stats.f_oneway(*flat_data)
        df_for_export['F statistic'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'F statistic'] = statistics[0]

        df_for_export['p value'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'p value'] = statistics[1]

        if stats.f_oneway(*flat_data)[-1] < 0.05:
            # perform multiple pairwise comparison (Tukey HSD)
            #tukey_export = sp.posthoc_tukey(
            #    df_for_stats,
            #    val_col=data_label,
            #    group_col=group_label)
            # This gave strange results and the maintainer of the
            # package said that they don't like the implementation:
            # https://github.com/maximtrp/scikit-posthocs/issues/60
            # instead, use the scipy test
            tukey_result = stats.tukey_hsd(*data)
            tukey_export = pd.DataFrame(columns=label_list_for_tukey)
            for i in range(len(label_list_for_tukey)):
                df_dictionary = pd.DataFrame([tukey_result.pvalue[:, i]],
                                             columns=label_list_for_tukey,
                                             index=[label_list_for_tukey[i]])
                tukey_export = pd.concat([tukey_export, df_dictionary])

    else:
        df_for_export.loc[0, 'statistical test'] = 'Kruskal-Wallis'
        statistics = stats.kruskal(*flat_data)

        # df_for_export['H statistic'] = [stats.kruskal(*flat_data)[0], np.nan,np.nan] # This is less flexible!
        df_for_export['H statistic'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'H statistic'] = statistics[0]

        df_for_export['p value'] = pd.Series(dtype='float')
        df_for_export.loc[0, 'p value'] = statistics[1]

        if stats.kruskal(*flat_data)[1] < 0.05:
            conover_export = sp.posthoc_conover(
                df_for_stats,
                val_col=data_label,
                group_col=group_label,
                p_adjust='holm')

    if statistics[1] < 0.05:
        df_for_export.loc[0, 'significant'] = 'YES'
    else:
        df_for_export.loc[0, 'significant'] = 'NO'

    if parametric:
        df_for_export.to_csv(Path(savepath, savename +'_ANOVA.csv'),index=False)
        try:
            tukey_export.to_csv(Path(savepath, savename + '_Tukey.csv'),
                                  index=True)
        except NameError:
            pass
    else:
        df_for_export.to_csv(Path(savepath, savename + '_kruskal.csv'),
                             index=False)
        try:
            conover_export.to_csv(Path(savepath, savename + '_conover.csv'),
                                  index=True)
        except NameError:
            pass

    print(df_for_export)
    print('\n')
    try:
        print(conover_export)
    except NameError:
        pass

    try:
        print(tukey_export)
    except NameError:
        pass

def confidence_interval(dataset, level=0.95):
    """
    Calculate confidence interval for one sample.
    """
    return(stats.t.interval(level, dataset.shape[0]-1, loc=np.mean(dataset), scale=stats.sem(dataset)))

def Holm_bonferroni_pvalue_correction(p_values):
    """
    Performs Holm-Bonferroni multiple test correction.

    Note that once a corrected p_value is larger than 0.05 the current
    correction factor is kept for the rest of the dataset.
    This is to ensure that one a non-significant datapoint has been found
    all datapoints with larger p-values by definition must be non-significant.

    For example for p-values 0.04, 0.03 and 0.01 the corrected values are:
    0.01 * 3 = 0.03*, 0.03 * 2 = 0.06ns.
    If this were <0.05 the 0.04 would be  0.04 * 1 = 0.04 and therefore significant.
    However, BECAUSE 0.03 * 2 = 0.06ns, 0.04 automatically is not significant.!
    """

    p_values_np = np.array(p_values)

    corrected_p_values = p_values_np.copy()  # To not overwrite the original p_values!

    update_correction_factor = True  # only update correction factor if previous iteration was significant!!
    for counter, current_rank in enumerate(np.argsort(p_values_np)):
        # print(current_rank)
        # this should start at the lowest p-value

        # current_alpha = 0.05 / (p_values_np.shape[0] - counter + 1)

        # Note: the full formula is for corrected p_value is: (n – rank + 1)
        # However, because counter starts at 0 for the lowest rank (instead of 1)
        # I ommit the "+1" and the end of the formula!
        if update_correction_factor:
            current_correction_factor = p_values_np.shape[0] - counter
            # print(current_correction_factor)

        corrected_p_values[current_rank] = current_correction_factor * p_values_np[current_rank]

        if corrected_p_values[current_rank] < 0.05:
            update_correction_factor = True
        else:
            update_correction_factor = False

    return (corrected_p_values)

def flatten_list(nested_list):
    """
    Convenience function to flatten a list
    """
    flat_list = []
    for sublist in nested_list:
        for element in sublist:
            flat_list.append(element)
    return(flat_list)

def export_pairwise_test(data, label_list,
                         savepath, savename,
                         parametric=True):
    """
    This function can be used for a single or multiple pairwise tests.

    In case of multiple tests (identified as nested lists),
    Bonferroni correction is applied and both the original and
    corrected p-value is reported.
    """

    # First, collect the data of variable names below
    test_statistic = []
    p_values = []
    all_labels = []
    mean = []
    median = []
    std = []
    sem = []
    n = []
    ci = []
    #effect_size = []
    dof = []

    no_of_groups = 0

    for counter, current_dataset in enumerate(data):
        # Explicitly assign left and right group of current data to be compared
        group1_data = current_dataset[0]
        group2_data = current_dataset[1]

        mean.append([np.nanmean(group1_data),
                     np.nanmean(group2_data)])

        median.append([np.nanmedian(group1_data),
                       np.nanmedian(group2_data)])

        std.append([np.std(group1_data),
                    np.std(group2_data)])

        n.append([group1_data.shape[0],
                  group2_data.shape[0]])

        sem.append([np.std(group1_data) / np.sqrt(group1_data.shape[0]),
                    np.std(group2_data) / np.sqrt(group2_data.shape[0])
                    ])

        ci.append([confidence_interval(group1_data),
                   confidence_interval(group2_data)])

        if parametric:
            test_results = stats.ttest_ind(group1_data, group2_data)
        else:
            test_results = stats.ranksums(group1_data, group2_data)
        test_statistic.append(test_results[0])
        p_values.append(test_results[-1])
        all_labels.append(label_list[counter])

        dof.append(sum(n[-1]) - 2) # n1+n2 - 2 for two sample tests

        no_of_groups += 1

    ###################################
    # Multiple Test correction using Holm-Bonferroni methods
    ###################################

    if len(p_values) > 1:
        corrected_p_values = Holm_bonferroni_pvalue_correction(p_values)

    #########################################
    # Finally, save the dataframe for export
    #########################################
    df_for_export = pd.DataFrame()

    flat_label_list = flatten_list(all_labels)
    df_for_export["Group"] = flat_label_list

    flat_n = flatten_list(n)
    df_for_export['n'] = flat_n

    flat_mean = flatten_list(mean)
    df_for_export['mean'] = flat_mean

    flat_median = flatten_list(median)
    df_for_export['median'] = flat_median

    flat_std = flatten_list(std)
    df_for_export['STD'] = flat_std

    flat_sem = flatten_list(sem)
    df_for_export['SEM'] = flat_sem

    flat_ci = flatten_list(ci)
    df_for_export['95% CI on the mean'] = flat_ci

    # Next, add values that are only added for every second row as we are always comparing two rows to each other
    for counter, current_index in enumerate(range(0, df_for_export.shape[0], 2)):  # only every second row

        df_for_export.loc[current_index, 'DOF'] = dof[counter]

        if parametric:
            df_for_export.loc[current_index, 'statistical test'] = 'T-test'
        else:
            df_for_export.loc[
                current_index, 'statistical test'] = 'Ranksums test'
        if parametric:
            df_for_export.loc[current_index, 't-statistic'] = test_statistic[counter]
        else:
            df_for_export.loc[current_index, 'rank sum statistic'] = test_statistic[counter]

        df_for_export.loc[current_index, 'p-value'] = p_values[counter]
        if len(p_values) > 1:
            df_for_export.loc[current_index, 'Holm-Bonferroni corrected p-value'] = \
                corrected_p_values[counter]

        # That will still work as expected as the corrected value will
        # be correct even if there's only one group!
        if len(p_values) > 1:
            if corrected_p_values[counter] < 0.05:
                df_for_export.loc[current_index, 'significant'] = 'YES'
                '''
                # Only put effect size in case of significant differences
                # of the group
                if parametric:
                    df_for_export.loc[current_index, "Effect size: Cohen's D"] = effect_size[counter]
                else:
                    df_for_export.loc[current_index, "Effect size: r^2"] = effect_size[counter]
                '''
            else:
                df_for_export.loc[current_index, 'significant'] = 'NO'
                '''
                # Only put effect size in case of significant differences
                # of the group
                df_for_export.loc[current_index, "Effect size"] = 'N/A'
                '''
        else:
            if p_values[counter] < 0.05:
                df_for_export.loc[current_index, 'significant'] = 'YES'
            else:
                df_for_export.loc[current_index, 'significant'] = 'NO'

    # if SAVE_PLOTS:
    if parametric:
        df_for_export.to_csv(Path(savepath, savename + '_t_test.csv'))
    else:
        df_for_export.to_csv(Path(savepath, savename + '_ranksums.csv'))

    print(df_for_export)
